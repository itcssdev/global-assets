/**
   * https://stackoverflow.com/questions/13798313/set-custom-html5-required-field-validation-message
  * @author ComFreek
  * @license MIT (c) 2013-2015 ComFreek <http://stackoverflow.com/users/603003/comfreek>
  * Please retain this author and license notice!
  */
// (function (exports) {
//     function valOrFunction(val, ctx, args) {
//         if (typeof val == "function") {
//             return val.apply(ctx, args);
//         } else {
//             return val;
//         }
//     }

//     function InvalidInputHelper(input, options) {
//         input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

//         function changeOrInput() {
//             if (input.value == "") {
//                 input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
//             } else {
//                 input.setCustomValidity("");
//             }
//         }

//         function invalid() {
//             if (input.value == "") {
//                 input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
//             } else {
//                input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
//             }
//         }

//         input.addEventListener("change", changeOrInput);
//         input.addEventListener("input", changeOrInput);
//         input.addEventListener("invalid", invalid);
//     }
//     exports.InvalidInputHelper = InvalidInputHelper;
// })(window);



// InvalidInputHelper(document.getElementById("email"), {
//     defaultText: "Please enter an email address!",
//     emptyText: "Please enter an email address!",
//     invalidText: function (input) {
//         return 'The email address "' + input.value + '" is invalid!';
//     }
// });


/**
  * https://css-tricks.com/form-validation-part-2-constraint-validation-api-javascript/
  * Whenever a user leaves a field, we want to check if it's valid. To do this,
  * we'll setup an event listener.Rather than add a listener to every form field,
  * we'll use a technique called event bubbling (or event propagation) to listen
  * for all blur events.
  *
*/
// Add the novalidate attribute when the JS loads
var forms = document.querySelectorAll('.udit-form--validate');
for (var i = 0; i < forms.length; i++) {
    forms[i].setAttribute('novalidate', true);
}


// Validate the field
var hasError = function (field) {

    // Don't validate submits, buttons, file and reset inputs, and disabled fields
    if (field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') return;

    // Get validity
    var validity = field.validity;

    // If valid, return null
    if (validity.valid) return;

    // If field is required and empty
    if (validity.valueMissing) field.setAttribute("class", "error_border"); // return 'Please fill out this field.';

    // If not the right type
    if (validity.typeMismatch) {

        // Email
        if (field.type === 'email') return 'Please enter an email address.';

        // URL
        if (field.type === 'url') return 'Please enter a URL.';

    }

    // If too short
    if (validity.tooShort) return 'Please lengthen this text to ' + field.getAttribute('minLength') + ' characters or more. You are currently using ' + field.value.length + ' characters.';

    // If too long
    if (validity.tooLong) return 'Please shorten this text to no more than ' + field.getAttribute('maxLength') + ' characters. You are currently using ' + field.value.length + ' characters.';

    // If number input isn't a number
    if (validity.badInput) return 'Please enter a number.';

    // If a number value doesn't match the step interval
    if (validity.stepMismatch) return 'Please select a valid value.';

    // If a number field is over the max
    if (validity.rangeOverflow) return 'Please select a value that is no more than ' + field.getAttribute('max') + '.';

    // If a number field is below the min
    if (validity.rangeUnderflow) return 'Please select a value that is no less than ' + field.getAttribute('min') + '.';

      // If pattern doesn't match
    if (validity.patternMismatch) {

        // If pattern info is included, return custom error
        if (field.hasAttribute('pattern')) return field.getAttribute('pattern');

        // Otherwise, generic error
        return 'Please match the requested format.';

    }

    // If all else fails, return a generic catchall error
    return 'The value you entered for this field is invalid.';

};


// Show an error message
var showError = function (field, error) {

    // Add error class to field
    field.classList.add('error');

    // If the field is a radio button and part of a group, error all and get the last item in the group
    if (field.type === 'radio' && field.name) {
        var group = document.getElementsByName(field.name);
        if (group.length > 0) {
            for (var i = 0; i < group.length; i++) {
                // Only check fields in current form
                if (group[i].form !== field.form) continue;
                group[i].classList.add('error');
            }
            field = group[group.length - 1];
        }
    }

    // Get field id or name
    var id = field.id || field.name;
    if (!id) return;

    // Check if error message field already exists
    // If not, create one
    var message = field.form.querySelector('.error-message#error-for-' + id );
    if (!message) {
        message = document.createElement('div');
        message.className = 'error-message';
        message.id = 'error-for-' + id;

        // If the field is a radio button or checkbox, insert error after the label
        var label;
        if (field.type === 'radio' || field.type ==='checkbox') {
            label = field.form.querySelector('label[for="' + id + '"]') || field.parentNode;
            if (label) {
                label.parentNode.insertBefore( message, label.nextSibling );
            }
        }

        // Otherwise, insert it after the field
        if (!label) {
            field.parentNode.insertBefore( message, field.nextSibling );
        }

    }

    // Add ARIA role to the field
    field.setAttribute('aria-describedby', 'error-for-' + id);

    // Update error message
    message.innerHTML = error;

    // Show error message
    message.style.display = 'block';
    message.style.visibility = 'visible';

};


// Remove the error message
var removeError = function (field) {

    // Remove error class to field
    field.classList.remove('error');

    // Remove ARIA role from the field
    field.removeAttribute('aria-describedby');

    // If the field is a radio button and part of a group, remove error from all and get the last item in the group
    if (field.type === 'radio' && field.name) {
        var group = document.getElementsByName(field.name);
        if (group.length > 0) {
            for (var i = 0; i < group.length; i++) {
                // Only check fields in current form
                if (group[i].form !== field.form) continue;
                group[i].classList.remove('error');
            }
            field = group[group.length - 1];
        }
    }

    // Get field id or name
    var id = field.id || field.name;
    if (!id) return;


    // Check if an error message is in the DOM
    var message = field.form.querySelector('.error-message#error-for-' + id + '');
    if (!message) return;

    // If so, hide it
    message.innerHTML = '';
    message.style.display = 'none';
    message.style.visibility = 'hidden';

};


// Listen to all blur events
document.addEventListener('blur', function (event) {

    // Only run if the field is in a form to be validated
    if (!event.target.classList.contains('udit-form--validate')) return;

    // Validate the field
    var error = hasError(event.target);

    // If there's an error, show it
    if (error) {
        showError(event.target, error);
        return;
    }

    // Otherwise, remove any existing error message
    removeError(event.target);

}, true);


// Check all fields on submit
document.addEventListener('submit', function (event) {

    // Only run on forms flagged for validation
    if (!event.target.classList.contains('udit-form--validate')) return;

    // Get all of the form elements
    var fields = event.target.elements;

    // Validate each field
    // Store the first field with an error to a variable so we can bring it into focus later
    var error, hasErrors;
    for (var i = 0; i < fields.length; i++) {
        error = hasError(fields[i]);
        if (error) {
            showError(fields[i], error);
            if (!hasErrors) {
                hasErrors = fields[i];
            }
        }
    }

    // If there are errrors, don't submit form and focus on first element with error
    if (hasErrors) {
        event.preventDefault();
        hasErrors.focus();
    }

    // Otherwise, let the form submit normally
    // You could also bolt in an Ajax form submit process here

}, false);

/**
 * MOBILE AUTOCOMPLETE ATTRIBUTES
 *
 * @description     Checks UA string for "mobi". If true, sets autofill on specific fields.
 * @see            https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#autofill
 *
 * IE 11 doesn't support includes(). Use indexOf for IE 11
*/
// const isMobileDevice = window.navigator.userAgent.toLowerCase().includes("mobi");
// const isMobileDevice = window.navigator.userAgent.toLowerCase().indexOf("mobi");
// if ( isMobileDevice ) {
//   const nodelist = document.querySelectorAll('.ac');
//   const thingsArray = Array.from(nodelist);
//   var fields = thingsArray.forEach(function (thing) {
//     if (thing.type == "email") {
//       thing.setAttribute("autocomplete", "email");
//       return true;
//     }
//     else if (thing.type == "tel") {
//       thing.setAttribute("autocomplete", "tel");
//       return true;
//     }
//     else if (thing.name == "fname" ) {
//       thing.setAttribute("autocomplete", "given-name");
//     }
//     else if (thing.name == "lname" ) {
//       thing.setAttribute("autocomplete", "family-name");
//     }
//   //thing => console.log('fieldZs' + thing.type);
//   });
// }
/**
 * EMAIL SUFFIX AUTOCOMPLETE
 *
 * @description    Offers users a quick selection for email suffixes
 * @see           https://codepen.io/mildrenben/pen/WvGEzm?editors=0010
 */
var email = document.querySelector( '#emailAddr' ),
    auto = document.querySelector( '.autosuffix'),
    popularEmails = ['udel.edu', 'gmail.com', 'outlook.com'],
    itemSelected = 0,
    itemList = [];

window.addEventListener('keyup', function(){
  if( window.event.keyCode === 40 ) { // Down
    if( itemSelected ===  ( itemList.length - 1 ) ) {
      itemSelected = itemList.length - 1;
    }
    else {
      itemSelected += 1;
    }
  }

  if(window.event. keyCode === 38 ) { // Up
    if( itemSelected === 0 ) {
      return;
    }
    else {
      itemSelected -= 1;
    }
  }

  if( window.event.keyCode === 13 ) { // Enter
    email.value = itemList[itemSelected].textContent;
    auto.innerHTML = '';
  }

  for( var i = 0; i < itemList.length; i++ ) { // For loop through all items and add selected class if needed
    if( itemList[i].classList.contains( 'selected' ) ) {
      itemList[i].classList.remove('selected');
    }
    if( itemSelected === i ) {
      itemList[i].classList.add( 'selected' );
    }
  }

  console.log(itemSelected, itemList);
});


email.addEventListener('keyup', function() {
  auto.innerHTML = '';

  if(email.value.match('@')) { // If the input has a @ in it
    var afterAt = email.value.substring(email.value.indexOf('@') + 1, email.value.length);
    var popularEmailsSub = [];

    for(var l = 0; l < popularEmails.length; l++) {
      popularEmailsSub.push(popularEmails[l].substring(0, afterAt.length))
    }

    if(afterAt == '') {
      for(var i = 0; i < popularEmails.length; i++) {
        auto.innerHTML += '<li>' + email.value + popularEmails[i] + '</li>';
      }
      itemList = document.querySelectorAll('.autosuffix li');
      itemList[0].classList.add('selected');
    }

    else if(!(afterAt == '')) {
      var matchedEmails = [];

      for(var k = 0; k < popularEmails.length; k++) {
        if(popularEmailsSub[k].match(afterAt)) {
          matchedEmails.push(popularEmails[k]);
        }
      }

      for(var i = 0; i < matchedEmails.length; i++) {
        auto.innerHTML += '<li>' + email.value.substring(0, email.value.indexOf('@')) + '@' + matchedEmails[i] + '</li>';
      }
    }

    var itemsList = document.querySelectorAll('.autosuffix li');

    for(var j = 0; j < itemsList.length; j++) {
      itemsList[j].addEventListener('click', function() {
        email.value = this.textContent;
        auto.innerHTML = '';
      });
    }
  }
});
